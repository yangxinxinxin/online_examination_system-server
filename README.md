
在线考试系统（服务端API）

- 毕业设计
- 测试数据采集自本地数据库
- 服务端api基于Ｎode.js+ThinkJS+MySQL

注意：当前版本功能还未完善，请勿商用。 本项目需要配合在线考试（微信小程序) https://gitee.com/qinxuyu/online_examination_system_miniprogram
在线考试（后台管理）https://gitee.com/qinxuyu/online_examination_system_admin
## Install dependencies

## 本地开发环境配置

- 克隆项目到本地

```
git clone https://gitee.com/qinxuyu/online_examination_system-server.git

```

- 创建数据库exam并导入项目根目录下的exam.sql,数据库字符编码为utf8。
```
CREATE SCHEMA `nideshop` DEFAULT CHARACTER SET utf8;

```

- 更改数据库配置 src/common/config/db.js
```
'use strict';
/**
 * db config
 * @type {Object}
 */
export default {
  type: 'mysql',
  adapter: {
    mysql: {
      host: '127.0.0.1',
      port: '3306',
      database: 'exam',
      user: 'root',
      password: '你的密码',
      prefix: 'tb_',
      encoding: 'utf8'
    },
    mongo: {
    }
  }
};

```

- 安装依赖并启动
```
npm install
npm start
```

访问http://127.0.0.1:8360/