﻿const Base = require("./base.js");

module.exports = class extends Base {
  /**
   * 获取老师所教授的专业和课程
   */
  async getProCorAction() {
    const teacher_no = this.post("teacher_no");

    const sql =
      "SELECT tb_relevance.relevance_no,teacher_no,tb_relevance.profession_no,tb_relevance.course_no,profession_name,course_name FROM tb_relevance  JOIN tb_profession ON(tb_relevance.profession_no=tb_profession.profession_no) JOIN tb_course ON (tb_relevance.course_no=tb_course.course_no) WHERE teacher_no='" +
      teacher_no +
      "';";
    const relevanceList = await this.model("relevance").query(sql);

    return this.success({
      relevanceList: relevanceList
    });
  }

  /**
   * 获取教师个人信息
   */
  async getTeacherInforAction() {
    const teacher_no = this.post("teacher_no");

    const teacherInfor = await this.model("teacher").where({
      teacher_no: teacher_no
    }).find();

    const sql = "SELECT * FROM tb_profession";
    const professionList = await this.model("profession").query(sql);

    const sql_2 = "SELECT * FROM tb_course";
    const courseList = await this.model("course").query(sql_2);

    return this.success({
      teacherInfor: teacherInfor,
      professionList: professionList,
      courseList: courseList
    });
  }

  /**
   * 删除教师的授课专业和课程
   */
  async deleteRelevanceAction() {
    const relevance_no = this.post("relevance_no");

    await this.model("relevance")
      .where({
        relevance_no: relevance_no
      })
      .delete();

    return this.success({
      code: 100
    });
  }

  /**
   * 添加授课的专业和课程
   */
  async addRelevanceAction() {
    const teacher_no = this.post("teacher_no");
    const profession_no = this.post("profession_no");
    const course_no = this.post("course_no");

    await this.model("relevance").add({
      teacher_no: teacher_no,
      profession_no: profession_no,
      course_no: course_no
    });

    return this.success({
      code: 100
    });
  }

  /**
   * 修改密码
   */
  async changePassWordAction() {
    const teacher_no = this.post("teacher_no");
    const oldPassword = this.post("oldPassword");
    const newPassWord = this.post("newPassWord");

    const data = await this.model("teacher")
      .where({
        teacher_no: teacher_no,
        teacher_pw: oldPassword
      })
      .find();

    if (think.isEmpty(data)) {
      return {
        code: 101
      };
    }

    await this.model("teacher")
      .where({
        teacher_no: teacher_no
      })
      .update({
        teacher_pw: newPassWord
      });

    return this.success({
      code: 100
    });
  }

  /**
   * 修改教师个人信息
   */
  async changeTeacherInforAction() {
    const old_no = this.post('old_no');
    const teacher_no = this.post('teacher_no');
    const teacher_name = this.post('teacher_name');
    const teacher_phone = this.post('teacher_phone');

    await this.model('relevance').where({
      teacher_no: old_no
    }).update({
      teacher_no: teacher_no
    });

    await this.model('teacher').where({
      teacher_no: old_no
    }).update({
      teacher_no: teacher_no,
      teacher_name: teacher_name,
      teacher_phone: teacher_phone,
      teacher_email: teacher_no + "@nuist.edu.cn"
    });

    return this.success({
      code: 100
    });
  }
};