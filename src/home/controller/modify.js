const Base = require("./base.js");

module.exports = class extends Base {
    /**
     * 批改答题记录
     */
    async modifyAction() {
        const stu_no = this.post("stu_no");
        const profession_no = this.post("profession_no");
        const course_no = this.post("course_no");

        const zuodaInfo = this.post("zuodaInfo");
        const mulZuoDaInfo = this.post("mulZuoDaInfo");
        const zhuguanZuoDaInfo = this.post("zhuguanZuoDaInfo");

        const mark1 = this.post("mark1");
        const mark2 = this.post("mark2");

        // 计算单选题得分
        let total1;
        if (zuodaInfo.length === 0) {
            total1 = 0;
        } else {
            total1 = this.total1(profession_no, course_no, stu_no, zuodaInfo, mark1);
        }

        // 计算多选题得分
        let total2;
        if (mulZuoDaInfo.length === 0) {
            total2 = 0;
        } else {
            total2 = this.mulTotal(profession_no, course_no, stu_no, mulZuoDaInfo, mark2);
        }

        // 添加成绩信息
        await this.model("mark").add({
            student_no: stu_no,
            profession_no: profession_no,
            course_no: course_no,
            total1: total1,
            total2: total2
        });

        // 保存主观题作答
        if (zhuguanZuoDaInfo.length != 0) {
            await this.addZhuguanRecords(profession_no, course_no, stu_no, zhuguanZuoDaInfo);
        }

        return this.success({
            code: 100
        });
    }

    /**
     * 计算单选题总分
     */
    total1(profession_no, course_no, stu_no, zuodaInfo, mark1) {
        let rightNum = 0;
        let total = 0;

        for (let i = 0; i < zuodaInfo.length; i++) {
            if (zuodaInfo[i].goodkey === zuodaInfo[i].selectOption.toString()) {
                rightNum++;
                this.model('keguanrecords').add({
                    profession_no: profession_no,
                    course_no: course_no,
                    stu_no: stu_no,
                    que_no: zuodaInfo[i].que_no,
                    goodkey: zuodaInfo[i].goodkey,
                    selectOption: zuodaInfo[i].selectOption,
                    type: '单选题',
                    mark: mark1
                });
            } else {
                this.model('keguanrecords').add({
                    profession_no: profession_no,
                    course_no: course_no,
                    stu_no: stu_no,
                    que_no: zuodaInfo[i].que_no,
                    goodkey: zuodaInfo[i].goodkey,
                    selectOption: zuodaInfo[i].selectOption,
                    type: '单选题',
                    mark: 0
                });
            }
        }
        total = rightNum * mark1;
        return total;
    }


    /**
     * 计算机多选题总分
     */
    mulTotal(profession_no, course_no, stu_no, mulZuoDaInfo, mark2) {
        let rightNum = 0;
        let mulTotal = 0;

        for (let i = 0; i < mulZuoDaInfo.length; i++) {
            if (mulZuoDaInfo[i].goodkey === mulZuoDaInfo[i].mulSelectOption.sort().toString()) {
                rightNum++;
                this.model('keguanrecords').add({
                    profession_no: profession_no,
                    course_no: course_no,
                    stu_no: stu_no,
                    que_no: mulZuoDaInfo[i].que_no,
                    goodkey: mulZuoDaInfo[i].goodkey,
                    selectOption: mulZuoDaInfo[i].mulSelectOption.sort().toString(),
                    type: '多选题',
                    mark: mark2
                });
            } else {
                this.model('keguanrecords').add({
                    profession_no: profession_no,
                    course_no: course_no,
                    stu_no: stu_no,
                    que_no: mulZuoDaInfo[i].que_no,
                    goodkey: mulZuoDaInfo[i].goodkey,
                    selectOption: mulZuoDaInfo[i].mulSelectOption.sort().toString(),
                    type: '多选题',
                    mark: 0
                });
            }
        }
        mulTotal = mark2 * rightNum;
        return mulTotal;
    }

    /**
     * 录入主观题答题记录
     */
    async addZhuguanRecords(profession_no, course_no, stu_no, zhuguanZuoDaInfo) {

        for (let i = 0; i < zhuguanZuoDaInfo.length; i++) {
            this.model("zhuguanrecords").add({
                profession_no: profession_no,
                course_no: course_no,
                stu_no: stu_no,
                que_no: zhuguanZuoDaInfo[i].que_no,
                context: zhuguanZuoDaInfo[i].context
            });
        }
    }
};