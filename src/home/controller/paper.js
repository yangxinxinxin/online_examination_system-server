const Base = require("./base.js");

module.exports = class extends Base {
    /**
     * 获取考试列表
     */
    async getPaperListAction() {
        const profession = this.post("profession");

        const sql =
            "SELECT * FROM tb_paper  LEFT JOIN tb_profession ON (tb_paper.profession_no=tb_profession.profession_no) LEFT JOIN tb_course on (tb_paper.course_no=tb_course.course_no) WHERE profession_name=" +
            "'" +
            profession +
            "' AND statue='2' or statue='4';";
        let tmpPaper = await this.model("paper").query(sql);

        let paper = await this.checkTime(tmpPaper);
        return this.success({
            paper: paper
        });
    }

    /**
     * 检查是否重复参加考试
     */
    async againTestAction() {
        const profession_no = this.post('profession_no');
        const course_no = this.post('course_no');
        const student_no = this.post('student_no');

        const sql = "SELECT * FROM tb_mark WHERE profession_no='" + profession_no + "' AND course_no='" + course_no + "' AND student_no='" + student_no + "';";

        const data = await this.model("mark").query(sql);

        if (!(think.isEmpty(data))) {
            return this.fail({
                code: 101
            });
        }

        return this.success({
            code: 100
        });
    }

    /**
     * 通过专业和课程获取考试列表
     */
    async getPaperByProCouAction() {
        const profession_no = this.post("profession_no");
        const course_no = this.post("course_no");

        const sql =
            "SELECT paper_no,profession_name,course_name,num1,mark1,num2,mark2,num3,mark3,mark,stime,etime,statue FROM tb_paper  LEFT JOIN tb_profession ON (tb_paper.profession_no=tb_profession.profession_no) LEFT JOIN tb_course on (tb_paper.course_no=tb_course.course_no) WHERE tb_paper.profession_no='" +
            profession_no +
            "' AND tb_paper.course_no='" +
            course_no +
            "' ORDER BY stime DESC;";
        let tmpPaperList = await this.model("paper").query(sql);
        let paperList = await this.checkTime(tmpPaperList);
        return this.success({
            paperList: paperList
        });
    }

    /**
     * 申请考试
     */
    async addPaperAction() {
        const paper = JSON.parse(this.post('paper'));
        await this.model('paper').add({
            profession_no: paper.profession_no,
            course_no: paper.course_no,
            num1: paper.num1,
            mark1: paper.mark1,
            num2: paper.num2,
            mark2: paper.mark2,
            num3: paper.num3,
            mark3: paper.mark3,
            mark: paper.mark,
            stime: paper.stime,
            etime: paper.etime,
            statue: paper.statue
        });
        return this.success({
            code: 100
        });
    }


    /**
     * 获取全部考试
     */
    async allPaperAction() {

        const sql = "SELECT * FROM tb_paper JOIN tb_profession ON (tb_paper.profession_no=tb_profession.profession_no) JOIN tb_course ON(tb_paper.course_no=tb_course.course_no) ORDER BY stime ASC;";
        let tmpPaperList = await this.model("paper").query(sql);

        let paperList = await this.checkTime(tmpPaperList);

        return this.success({
            paperList: paperList
        });
    }

    /**
     * 检查当前时间和考试时间设置的关系,从而修改考试状态
     */
    async checkTime(paperList) {
        let nowTime = Date.parse(new Date());
        let tmpPaperList = paperList;
        for (let i = 0; i < tmpPaperList.length; i++) {

            let startTime = Date.parse(tmpPaperList[i].stime);
            let endTime = Date.parse(tmpPaperList[i].etime);
            //考试进行中
            if ((nowTime >= startTime) && (endTime > nowTime) && (tmpPaperList[i].statue !== 4)) {
                tmpPaperList[i].statue = 4;
                await this.model("paper").where({
                    paper_no: tmpPaperList[i].paper_no
                }).update({
                    statue: 4
                });
            }
            //考试已结束
            else if (endTime < nowTime && tmpPaperList[i].statue !== 5) {
                tmpPaperList[i].statue = 5;
                await this.model("paper").where({
                    paper_no: tmpPaperList[i].paper_no
                }).update({
                    statue: 5
                });
            }
        }
        return tmpPaperList;
    }

    /**
     * 修改考试状态(批准或者驳回)
     */
    async changeStatueAction() {
        const paper_no = this.post('paper_no');
        const statue = this.post('statue');
        await this.model("paper").where({
            paper_no: paper_no
        }).update({
            statue: statue
        });
    }
};