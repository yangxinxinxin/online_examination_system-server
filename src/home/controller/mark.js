﻿const Base = require("./base.js");

module.exports = class extends Base {
    /**
     * 获取单个学生成绩信息
     */
    async getMarkListAction() {
        const stu_no = this.post('stu_no');
        // const sql = "SELECT course_name,total1,total2,total3,total FROM tb_mark JOIN tb_profession ON (tb_mark.profession_no=tb_profession.profession_no) JOIN tb_course ON (tb_mark.course_no=tb_course.course_no) WHERE tb_mark.student_no=" + "'" + stu_no + "';";

        const sql = "SELECT * FROM tb_mark JOIN tb_profession ON (tb_mark.profession_no=tb_profession.profession_no) JOIN tb_course ON (tb_mark.course_no=tb_course.course_no) WHERE tb_mark.student_no=" + "'" + stu_no + "';";

        const markList = await this.model('mark').query(sql);
        return this.success({
            markList: markList
        });
    }

    /**
     * 获取所有特定专业特定课程的学生成绩
     */
    async getSpecificMarkListAction() {
        const profession_no = this.post('profession_no');
        const course_no = this.post('course_no');
        const sql = "SELECT profession_name,course_name,tb_mark.student_no,tb_student.student_name,total1,total2,total3,total FROM tb_mark JOIN tb_profession ON (tb_mark.profession_no=tb_profession.profession_no) JOIN tb_course ON (tb_mark.course_no=tb_course.course_no) JOIN tb_student ON (tb_mark.student_no=tb_student.student_no) WHERE tb_mark.profession_no=" + "'" + profession_no + "'" + " AND tb_mark.course_no=" + "'" + course_no + "';";
        const specificMarkList = await this.model("mark").query(sql);
        return this.success({
            specificMarkList: specificMarkList
        });
    }
}