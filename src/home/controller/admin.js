const Base = require("./base.js");

module.exports = class extends Base {
  /**
   * 获取所有专业和课程
   */
  async allProCouAction() {
    const sql = "SELECT * FROM tb_profession JOIN tb_course;";
    const procouList = await this.model("profession").query(sql);

    return this.success({
      procouList: procouList
    });
  }

  /**
   * 获取所有学生的成绩
   */
  async allMarkListAction() {
    const sql =
      "SELECT profession_name,course_name,tb_mark.student_no,tb_student.student_name,total1,total2,total3,total FROM tb_mark JOIN tb_profession ON (tb_mark.profession_no=tb_profession.profession_no) JOIN tb_course ON(tb_course.course_no=tb_mark.course_no) JOIN tb_student ON (tb_mark.student_no=tb_student.student_no);";

    let markList = await await this.model("mark").query(sql);

    return this.success({
      markList: markList
    });
  }

  /**
   * 获取学生用户信息
   */
  async getStudentInforAction() {
    const sql =
      "SELECT student_no,student_name,student_email,student_phone,profession_name,tb_student.profession_no FROM tb_student JOIN tb_profession ON(tb_student.profession_no=tb_profession.profession_no);";

    let studentInfoList = await await this.model("student").query(sql);

    return this.success({
      studentInfoList: studentInfoList
    });
  }

  /**
   * 获取所有教师信息
   */
  async getTeacherInforAction() {
    const sql = "SELECT * FROM tb_teacher";

    let teacherInforList = await this.model("teacher").query(sql);

    return this.success({
      teacherInforList: teacherInforList
    });
  }

  /**
   * 获取管理员信息
   */
  async getInformationAction() {
    const admin_no = this.post("admin_no");

    const sql = "SELECT * FROM WHERE tb_admin.admin_no='" + admin_no + "';";

    let adminInfor = await this.model("admin")
      .where({
        admin_no: admin_no
      })
      .find();

    return this.success({
      adminInfor: adminInfor
    });
  }

  /**
   * 修改管理员密码
   */
  async changePassWordAction() {
    const oldpw = this.post("oldpw");
    const admin_no = this.post("admin_no");
    const admin_pw = this.post("admin_pw");

    let data = await this.model("admin")
      .where({
        admin_no: admin_no,
        admin_pw: oldpw
      })
      .find();

    if (think.isEmpty(data)) {
      return {
        code: 101
      };
    }
    const sql =
      "UPDATE tb_admin SET admin_pw='" +
      admin_pw +
      "' WHERE admin_no='" +
      admin_no +
      "';";
    await this.model("admin").query(sql);

    return this.success({
      code: 100
    });
  }

  /**
   * 删除学生信息
   */
  async deleteStudentAction() {
    const student_no = this.post("student_no");

    await this.model("zhuguanrecords")
      .where({
        stu_no: student_no
      })
      .delete();

    await this.model("keguanrecords")
      .where({
        stu_no: student_no
      })
      .delete();

    await this.model("mark")
      .where({
        student_no: student_no
      })
      .delete();

    await this.model("student")
      .where({
        student_no: student_no
      })
      .delete();

    return this.success({
      code: 100
    });
  }
  /**
   * 修改学生信息
   */
  async updateStudentAction() {
    const old_no = this.post("old_no");
    const student_no = this.post("student_no");
    const profession_no = this.post("profession_no");
    const student_name = this.post("student_name");
    const student_phone = this.post("student_phone");

    if (old_no !== student_no) {
      await this.model('mark').where({
        student_no: old_no
      }).update({
        student_no: student_no
      });

      await this.model('zhuguanrecords').where({
        stu_no: old_no
      }).update({
        stu_no: student_no
      });
    }
    await this.model('student')
      .where({
        student_no: old_no
      })
      .update({
        student_no: student_no,
        profession_no: profession_no,
        student_name: student_name,
        student_phone: student_phone,
        student_email: student_no + "@nuist.edu.cn"
      });

    return this.success({
      code: 100
    });
  }

  /**
   * 添加学生信息
   */
  async addStudentAction() {
    const student_no = this.post('student_no');
    const student_name = this.post('student_name');
    const student_pw = this.post('student_pw');
    const profession_no = this.post('profession_no');
    const student_phone = this.post('student_phone');

    const data = await this.model('student').where({
      student_no: student_no
    }).find();

    if (!(think.isEmpty(data))) {
      return this.fail({
        code: 101
      });
    }

    await this.model('student').add({
      student_no: student_no,
      profession_no: profession_no,
      student_name: student_name,
      student_phone: student_phone,
      student_email: student_no + "@edu.cn",
      student_pw: student_pw
    });
    return this.success({
      code: 100
    });
  }

  /**
   * 添加教师信息
   */
  async addTeacherAction() {
    const teacher_no = this.post('teacher_no');
    const teacher_name = this.post('teacher_name');
    const teacher_pw = this.post('teacher_pw');
    const teacher_phone = this.post('teacher_phone');

    const data = await this.model('teacher').where({
      teacher_no: teacher_no
    }).find();

    if (!(think.isEmpty(data))) {
      return {
        code: 101
      };
    }

    await this.model('teacher').add({
      teacher_no: teacher_no,
      teacher_name: teacher_name,
      teacher_pw: teacher_pw,
      teacher_phone: teacher_phone,
      teacher_email: teacher_no + '@nuist.edu.cn'
    });
    return this.success({
      code: 100
    });
  }

  /**
   * 删除教师信息
   */
  async deleteTeacherAction() {
    const teacher_no = this.post("teacher_no");

    await this.model("relevance")
      .where({
        teacher_no: teacher_no
      })
      .delete();

    await this.model("teacher")
      .where({
        teacher_no: teacher_no
      })
      .delete();

    return this.success({
      code: 100
    });
  }

  /**
   * 修改教师信息
   */
  async updateTeacherInforAction() {
    const old_no = this.post("old_no");
    const teacher_no = this.post("teacher_no");
    const teacher_name = this.post("teacher_name");
    const teacher_phone = this.post("teacher_phone");

    const data = await this.model('teacher').where({
      teacher_no: teacher_no
    }).find();

    if (!(think.isEmpty(data))) {
      return this.success({
        code: 101
      });
    }

    if (old_no != teacher_no) {
      await this.model('relevance')
        .where({
          teacher_no: old_no
        })
        .update({
          teacher_no: teacher_no
        });
    }

    await this.model('teacher').where({
      teacher_no: old_no
    }).update({
      teacher_no: teacher_no,
      teacher_name: teacher_name,
      teacher_phone: teacher_phone,
      teacher_email: teacher_no + '@nuist.edu.cn'
    });
    return this.success({
      code: 100
    });
  }

  /**
   * 修改教师密码
   */
  async updateTeacherPassWordAction() {
    const teacher_no = this.post('teacher_no');
    const teacher_pw = this.post('teacher_pw');

    await this.model('teacher').where({
      teacher_no: teacher_no
    }).update({
      teacher_pw: teacher_pw
    });

    return this.success({
      code: 100
    });
  }

  /**
   * 修改管理员信息
   */
  async changeAdminInforAction() {
    const old_no = this.post('old_no');
    const admin_no = this.post('admin_no');
    const admin_name = this.post('admin_name');
    const admin_phone = this.post('admin_phone');

    await this.model('admin').where({
      admin_no: old_no
    }).update({
      admin_no: admin_no,
      admin_name: admin_name,
      admin_phone: admin_phone,
      admin_email: admin_no + "@nuist.edu.cn"
    });

    return this.success({
      code: 100
    });
  }
}