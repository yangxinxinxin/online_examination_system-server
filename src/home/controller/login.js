const Base = require("./base.js");

module.exports = class extends Base {
    /**
     * 检查教师工号和密码
     */
    async chacNoPassWordAction() {
        const teacher_no = this.post('teacher_no');
        const teacher_pw = this.post('teacher_pw');

        let data = await this.model('teacher').where({
            teacher_no: teacher_no
        }).find();

        //无效帐号
        if (think.isEmpty(data)) {
            return this.fail({
                code: 101
            });
        }

        //密码错误
        if (data.teacher_pw !== teacher_pw) {
            return this.fail({
                code: 102
            });
        }

        return this.success({
            code: 100
        });
    }

    /**
     * 检查管理员工号和密码
     */
    async checkNoAdminPassWordAction() {
        const admin_no = this.post('admin_no');
        const admin_pw = this.post('admin_pw');

        let data = await this.model('admin').where({
            admin_no: admin_no
        }).find();

        //无效帐号
        if (think.isEmpty(data)) {
            return this.fail({
                code: 101
            });
        }

        //密码错误
        if (data.admin_pw !== admin_pw) {
            return this.fail({
                code: 102
            });
        }
        return this.success({
            code: 100
        });
    }
}