const Base = require("./base.js");

module.exports = class extends Base {
    /**
     * 获取详细的答题记录
     */
    async getDetailListAction() {
        const student_no = this.post('stu_no');
        const profession_no = this.post('profession_no');
        const course_no = this.post('course_no');

        const sql = "SELECT question,tb_keguanrecords.goodkey,selectOption,mark,tb_keguanrecords.type,A,B,C,D FROM tb_keguanrecords JOIN tb_question ON(tb_keguanrecords.que_no=tb_question.que_no) join tb_options on (tb_keguanrecords.que_no=tb_options.que_no) WHERE stu_no='" + student_no + "' AND tb_keguanrecords.profession_no='" + profession_no + "' AND tb_keguanrecords.course_no='" + course_no + "';";
        const sql_2 = "SELECT question,context,mark,tb_question.type FROM tb_zhuguanrecords JOIN tb_question ON (tb_zhuguanrecords.que_no=tb_question.que_no) WHERE stu_no='" + student_no + "' AND tb_zhuguanrecords.profession_no='" + profession_no + "' AND tb_zhuguanrecords.course_no='" + course_no + "';";

        const keguanlList = await this.model('keguanrecords').query(sql);
        const zhuguanList = await this.model('zhuguanrecords').query(sql_2);

        return this.success({
            detailList: keguanlList.concat(zhuguanList)
        });
    }
}