const Base = require("./base.js");

module.exports = class extends Base {
  /**
   * 查询试题数据（随机抽题）
   */
  async getQuestionAction() {
    const profession_no = this.post("profession_no");
    const course_no = this.post("course_no");
    const num1 = this.post("num1");
    const num2 = this.post("num2");
    const num3 = this.post("num3");

    const sql_1 =
      "SELECT que_no,question,type,goodkey FROM tb_question  LEFT JOIN tb_profession ON (tb_question.profession_no=tb_profession.profession_no) LEFT JOIN tb_course ON (tb_question.course_no=tb_course.course_no) WHERE tb_question.profession_no=" +
      "'" +
      profession_no +
      "'" +
      "AND tb_question.course_no=" +
      "'" +
      course_no +
      "' AND tb_question.type='单选题' ORDER BY RAND() LIMIT " +
      num1 +
      ";";

    let questions_1 = await this.model("question").query(sql_1);
    for (let i = 0; i < questions_1.length; i++) {
      let que_no = questions_1[i].que_no;
      await this.getOptionsAction(que_no).then(res => {
        questions_1[i].option = res[0];
      });
    }

    const sql_2 =
      "SELECT que_no,question,type,goodkey FROM tb_question  LEFT JOIN tb_profession ON (tb_question.profession_no=tb_profession.profession_no) LEFT JOIN tb_course ON (tb_question.course_no=tb_course.course_no) WHERE tb_question.profession_no=" +
      "'" +
      profession_no +
      "'" +
      "AND tb_question.course_no=" +
      "'" +
      course_no +
      "' AND tb_question.type='多选题' ORDER BY RAND() LIMIT " +
      num2 +
      ";";

    let questions_2 = await this.model("question").query(sql_2);
    for (let i = 0; i < questions_2.length; i++) {
      let que_no = questions_2[i].que_no;
      await this.getOptionsAction(que_no).then(res => {
        questions_2[i].option = res[0];
      });
    }

    const sql_3 =
      "SELECT que_no,question,type,goodkey FROM tb_question  LEFT JOIN tb_profession ON (tb_question.profession_no=tb_profession.profession_no) LEFT JOIN tb_course ON (tb_question.course_no=tb_course.course_no) WHERE tb_question.profession_no=" +
      "'" +
      profession_no +
      "'" +
      "AND tb_question.course_no=" +
      "'" +
      course_no +
      "' AND tb_question.type='主观题' ORDER BY RAND() LIMIT " +
      num3 +
      ";";

    let questions_3 = await this.model("question").query(sql_3);

    let questions = questions_1.concat(questions_2).concat(questions_3);

    return this.success({
      questions: questions
    });
  }

  /**
   * 根据专业和课程获取试题
   */
  async getQuestionByProCouAction() {
    const profession_no = this.post("profession_no");
    const course_no = this.post("course_no");

    const sql =
      "SELECT que_no,question,type,goodkey FROM tb_question  LEFT JOIN tb_profession ON (tb_question.profession_no=tb_profession.profession_no) LEFT JOIN tb_course ON (tb_question.course_no=tb_course.course_no) WHERE tb_question.profession_no=" +
      "'" +
      profession_no +
      "'" +
      "AND tb_question.course_no=" +
      "'" +
      course_no +
      "';";

    let questions = await this.model("question").query(sql);
    for (let i = 0; i < questions.length; i++) {
      let que_no = questions[i].que_no;
      await this.getOptionsAction(que_no).then(res => {
        questions[i].option = res[0];
      });
    }

    return this.success({
      questions: questions
    });
  }

  /**
   * 查询试题选项数据
   */
  async getOptionsAction(que_no) {
    const sql =
      // eslint-disable-next-line camelcase
      "SELECT A,B,C,D FROM tb_options WHERE que_no=" + "'" + que_no + "';";
    const options = await this.model("options").query(sql);
    return options;
  }

  /**
   * 添加试题
   */
  async addQuestionAction() {
    const questionInfo = JSON.parse(this.post("questionInfo"));

    const que_no = await this.model("question").add({
      profession_no: questionInfo.profession_no,
      course_no: questionInfo.course_no,
      question: questionInfo.question,
      type: questionInfo.type,
      goodkey: questionInfo.goodkey
    });

    await this.addOptions(que_no, questionInfo.options);
    // await this.getAgainQuetion(
    //   questionInfo.profession_no,
    //   questionInfo.course_no
    // );

    return this.success({
      code: 100
    });
  }

  /**
   * 添加试题选项
   */
  async addOptions(que_no, options) {
    this.model("options").add({
      que_no: que_no,
      A: options.A,
      B: options.B,
      C: options.C,
      D: options.D
    });
  }

  /**
   * 删除试题
   */
  async deleteQuestionAction() {
    const profession_no = this.post("profession_no");
    const course_no = this.post("course_no");
    const que_no = this.post("que_no");

    await this.model("zhuguanrecords")
      .where({
        que_no: que_no
      })
      .delete();
    await this.deleteOptions(que_no);
    await this.model("question")
      .where({
        que_no: que_no
      })
      .delete();

    return this.success({
      code: 100
    });
  }

  /**
   * 删除试题选项
   */
  async deleteOptions(que_no) {
    this.model("options")
      .where({
        que_no: que_no
      })
      .delete();
  }

  /**
   * 修改试题
   */
  async upDataQuestionAction() {
    const questionInfo = JSON.parse(this.post("questionInfo"));

    if (questionInfo.type != "主观题") {
      await this.upDataOption(questionInfo.que_no, questionInfo.options);
    }
    
    await this.model("question")
      .where({
        que_no: questionInfo.que_no
      })
      .update({
        question: questionInfo.question,
        type: questionInfo.type,
        goodkey: questionInfo.goodkey
      });

    // await this.getAgainQuetion(
    //   questionInfo.profession_no,
    //   questionInfo.course_no
    // );

    return this.success({
      code: 100
    });
  }

  /**
   * 修改试题选项
   */
  async upDataOption(que_no, options) {
    this.model("options")
      .where({
        que_no: que_no
      })
      .update({
        A: options.A,
        B: options.B,
        C: options.C,
        D: options.D
      });
  }

  /**
   * 变更试题后再次查询试题
   */
  async getAgainQuetion(profession_no, course_no) {
    const sql =
      "SELECT que_no,question,type,goodkey FROM tb_question  LEFT JOIN tb_profession ON (tb_question.profession_no=tb_profession.profession_no) LEFT JOIN tb_course ON (tb_question.course_no=tb_course.course_no) WHERE tb_question.profession_no=" +
      "'" +
      profession_no +
      "'" +
      "AND tb_question.course_no=" +
      "'" +
      course_no +
      "';";

    let questions = await this.model("question").query(sql);
    for (let i = 0; i < questions.length; i++) {
      let que_no = questions[i].que_no;
      await this.getOptionsAction(que_no).then(res => {
        questions[i].option = res[0];
      });
    }
    return this.success({
      questions: questions
    });
  }

  /**
   * 模糊查询试题
   */
  async searchQuestionAction() {
    let profession_no = this.post("profession_no");
    let course_no = this.post("course_no");
    let condition = this.post("condition");

    let sql =
      "SELECT * FROM tb_question WHERE profession_no='" +
      profession_no +
      "' AND course_no='" +
      course_no +
      "' AND tb_question.question LIKE '%" +
      condition +
      "%';";

    let questions = await this.model("question").query(sql);
    for (let i = 0; i < questions.length; i++) {
      let que_no = questions[i].que_no;
      await this.getOptionsAction(que_no).then(res => {
        questions[i].option = res[0];
      });
    }

    return this.success({
      questions: questions
    });
  }
};
