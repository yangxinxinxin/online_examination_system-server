'use strict';

// const Base = require("./base.js");

// module.exports = class extends Base {
// 	/**
// 	 * 查询用户是否已经注册
// 	 */
// 	async userAction() {
// 		const openid = this.post("openid");
// 		console.log(openid);
// 
// 		const sql =
// 			"SELECT * FROM tb_student  LEFT JOIN tb_profession ON (tb_student.profession_no=tb_profession.profession_no) WHERE student_openid=" +
// 			"'" +
// 			openid +
// 			"';";
// 		const userInfor = await this.model("student").query(sql);
// 
// 		if (think.isEmpty(userInfor)) {
// 			return this.fail({
// 				code: 101
// 			});
// 		}
// 
// 		return this.success({
// 			code: 100,
// 			userInfor: userInfor[0]
// 		});
// 	}
// 
// 	/**
// 	 * 绑定用户信息
// 	 */
// 	async bindAction() {
// 		const user = this.post("user");
// 
// 		const data = await this.model("student")
// 			.where({
// 				student_no: user.no
// 			})
// 			.find();
// 
// 		// 不存在的学号
// 		if (think.isEmpty(data)) {
// 			return this.fail({
// 				code: 101
// 			});
// 		}
// 		// 学号该已经通过验证
// 		if (data.student_openid != null) {
// 			return this.fail({
// 				code: 102
// 			});
// 		}
// 
// 		// 密码错误
// 		if (data.student_pw != user.password) {
// 			return this.fail({
// 				code: 103
// 			});
// 		}
// 
// 		await this.model("student")
// 			.where({
// 				student_no: user.no
// 			})
// 			.update({
// 				student_openid: user.openid
// 			});
// 		return this.success({
// 			code: 100
// 		});
// 	}
// };

import Base from './base.js';

export default class extends Base {
	async userAction() {
		const openid = this.post("openid");

		const sql =
			"SELECT * FROM tb_student  LEFT JOIN tb_profession ON (tb_student.profession_no=tb_profession.profession_no) WHERE student_openid=" +
			"'" +
			openid +
			"';";
		const userInfor = await this.model("student").query(sql);

		if (think.isEmpty(userInfor)) {
			return this.fail({
				code: 101
			});
		}

		return this.success({
			code: 100,
			userInfor: userInfor[0]
		});
	}

	/**
	 * 绑定用户信息
	 */
	async bindAction() {
		const user = this.post("user");

		const data = await this.model("student")
			.where({
				student_no: user.no
			})
			.find();

		// 不存在的学号
		if (think.isEmpty(data)) {
			return this.fail({
				code: 101
			});
		}
		// 学号该已经通过验证
		if (data.student_openid != null) {
			return this.fail({
				code: 102
			});
		}

		// 密码错误
		if (data.student_pw != user.password) {
			return this.fail({
				code: 103
			});
		}

		await this.model("student")
			.where({
				student_no: user.no
			})
			.update({
				student_openid: user.openid
			});
		return this.success({
			code: 100
		});
	}
}
